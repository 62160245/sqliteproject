package com.mycompany.sqliteproject;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


import java.sql.*;

public class ConnectDatabase {
  public static void main( String args[] ) {
      Connection c = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:test.db");
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Opened database successfully");
   }
}
