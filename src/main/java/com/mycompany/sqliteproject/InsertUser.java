package com.mycompany.sqliteproject;



import java.sql.*;

public class InsertUser {

   public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:test.db");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "INSERT INTO USERDATA (ID,NAME,AGE,ADDRESS,SEX) " +
                        "VALUES (1, 'Paul', 32, 'California', 'MALE' );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USERDATA (ID,NAME,AGE,ADDRESS,SEX) " +
                  "VALUES (2, 'Allen', 25, 'Texas', 'MALE' );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USERDATA (ID,NAME,AGE,ADDRESS,SEX) " +
                  "VALUES (3, 'LISA', 23, 'Norway', 'FEMALE' );"; 
         stmt.executeUpdate(sql);

         stmt.close();
         c.commit();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
   }
}